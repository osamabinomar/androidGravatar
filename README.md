# Android GravatarView

Android view to automatically generate avatars based on http://en.gravatar.com/ 

<div>
  <img src="https://gitlab.com/osamabinomar/androidGravatar/raw/master/sccren1.png" width="300" height="500"/>
</div>

## How To Use
### STEP 1
Add it to your build.gradle with:
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:

```gradle
dependencies {
     implementation 'com.gitlab.osamabinomar:androidGravatar:1.2'
}
```
### STEP 2
Add in your layout file. 
```    
 <com.bullhead.gravatar.GravatarView
        android:layout_width="150dp"
        android:layout_height="150dp"
        app:gravatar_email="m.osamabinomar@gmail.com"
        app:gravatar_circle="true"
        app:gravatar_type="robohash" />
        
```
## View attibutes

 Attribute                 | For What?                         | Values                                                                        |
| -------------------------|:---------------------------------:| :----------------------------------------------------------------------------:|
| `gravatar_type`                   | Types of avatars [See](http://en.gravatar.com/site/implement/images/)                 | `mp` , `identicon` , `monsterid` , `wavatar` , `retro` , `robohash` , `blank` |
| `gravatar_circle`                 | Circle image or not               | `true`,`false`                                                                |
| `gravatar_email`                  | User email to generate avatar for | Any email address                                                             |


**Example** `app:type="identicon"`
