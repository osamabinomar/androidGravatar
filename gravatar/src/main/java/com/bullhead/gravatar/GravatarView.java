package com.bullhead.gravatar;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IntRange;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import de.hdodenhof.circleimageview.CircleImageView;


public class GravatarView extends RelativeLayout {

    private static final String DEFAULT_EMAIL="androidgravatar@mail.com";

    private ImageView imageView;
    private CircleImageView circleImageView;

    public GravatarView(Context context) {
        super(context);
        init(context, null);
    }

    public GravatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GravatarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.GravatarView);

            boolean circle = attributes.getBoolean(R.styleable.GravatarView_gravatar_circle, false);
            int type = attributes.getInt(R.styleable.GravatarView_gravatar_type, Gravatar.DefaultImage.IDENTICON);
            String email = attributes.getString(R.styleable.GravatarView_gravatar_email);

            if (email==null){
                email=DEFAULT_EMAIL;
            }
            init(email,type,circle);
            attributes.recycle();
        } else {
            init(DEFAULT_EMAIL,Gravatar.DefaultImage.IDENTICON,false);
        }

    }

    @MainThread
    public void init(@NonNull String email, @IntRange(from = 0, to = 6) int type, boolean circle) {
        if (imageView == null) {
            inflateLayout();
        }

        this.imageView.setVisibility(circle?GONE:VISIBLE);
        this.circleImageView.setVisibility(circle?VISIBLE:GONE);

        String url = Gravatar.init()
                .with(email)
                .defaultImage(type)
                .size(Gravatar.MAX_IMAGE_SIZE_PIXEL)
                .build();
        Glide.with(getContext())
                .load(url)
                .into(circle?circleImageView:imageView);

    }

    private void inflateLayout() {
        LayoutInflater.from(getContext()).inflate(R.layout.gravater, this, true);
        this.imageView = findViewById(R.id.imageView);
        this.circleImageView = findViewById(R.id.circleImageView);
    }

    @MainThread
    public void setProfileImage(@NonNull String url) {
        if (this.circleImageView != null && circleImageView.getVisibility() == VISIBLE) {
            Glide.with(getContext())
                    .load(url)
                    .into(circleImageView);
        } else if (imageView != null && imageView.getVisibility() == VISIBLE) {
            Glide.with(getContext())
                    .load(url)
                    .into(imageView);
        }
    }


}
